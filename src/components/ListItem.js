import React, { Component } from 'react';
import {
  LayoutAnimation,
  Text,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import { connect } from 'react-redux';

import { CardSection } from './common';
import * as actions from '../actions';

class ListItem extends Component {
  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  renderDescription() {
    const { item, expanded } = this.props;
    if (expanded) {
      return (
        <CardSection>
          <Text style={styles.descriptionStyle}>
            {item.description}
          </Text>
        </CardSection>
      );
    }
  }

  render() {
    const { title, id } = this.props.item;
    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.selectLibrary(id)}
      >
        <View>
          <CardSection>
            <Text style={styles.titleStyle}>
              {title}
            </Text>
          </CardSection>
          {this.renderDescription()}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 23,
    fontWeight: '400',
    paddingLeft: 10
  },
  descriptionStyle: {
    flex: 1,
    fontSize: 18,
    padding: 10
  }
};

const mapStateToProps = ({ selectedLibraryId }, ownProps) => {
  const expanded = selectedLibraryId === ownProps.item.id;
  return { expanded };
};

export default connect(mapStateToProps, actions)(ListItem);

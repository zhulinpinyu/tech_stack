import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Header extends Component {
  render() {
    const { headerStyle, textStyle } = styles;
    return (
      <View style={headerStyle}>
        <Text style={textStyle}>
          {this.props.headerText}
        </Text>
      </View>
    );
  }
}

const styles = {
  headerStyle: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#f8f8f8',
    height: 60,
    paddingTop: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative',
  },
  textStyle: {
    fontSize: 20,
    color: '#0f0f0f',
    fontWeight: '600',
    paddingBottom: 6
  }
};

export { Header };
